# Git help
---
### Git - basic commands     

**Create a new local repository**    
`git init`     
**Check out a repository**    
`git clone path/to/repository`     
**Add files to staging**    
`git add <filename>` or `git add .`    
**Commit changes to HEAD (not to repository)**    
`git commit -m "Commit message"`    
**Send changes to the master branch of your remote repository after commit**    
`git push origin master`     
**List the files you've changed and those you still need to add or commit**     
`git status`        
**If you haven't connected your local repository to a remote server, add the server to be able to push to it**    
`git remote add origin <server>`     
**List all currently configured remote repositories**     
`git remote -v`     
**Create a new branch and switch right to it**     
`git checkout -b <branchName>`    
**Switch from one branch to another:**     
`git checkout <branchName>`    
**List all the branches in your repo, and also tell you what branch you're currently in**    
`git branch`    
**Delete the feature branch**     
`git checkout -d <branchName>`     
**Push changes to feature branch**     
`git push origin <branchName>`    
**Fetch and merge changes on the remote server to your working directory**     
`git pull`     
**To merge a different branch into your active branch**     
`git merge <branchName>`    
**View all the merge conflicts**    
`git diff`     
**View the conflicts against the base file**   
`git diff --base <filename>`    
**Preview changes, before merging**     
`git diff <sourcebranch> <targetbranch>`    
**If you mess up, you can replace the changes in your working tree with the last content in head: Changes already added to the index, as well as new files, will be kept**     
`git checkout -- <filename>`    
**Instead, to drop all your local changes and commits, fetch the latest history from the server and point your local master branch at it, do this**      
`git fetch origin`    
`git reset --hard origin/master`


